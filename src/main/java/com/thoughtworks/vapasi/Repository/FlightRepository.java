package com.thoughtworks.vapasi.Repository;

import com.thoughtworks.vapasi.models.Flight;
import com.thoughtworks.vapasi.models.FlightClass;
import com.thoughtworks.vapasi.models.FlightRoutes;
import com.thoughtworks.vapasi.models.FlightTripStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public class FlightRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private Map<Integer, Flight> flightData = new HashMap<>();
    private Map<Integer, FlightClass> flightClassData = new HashMap<>();
    private Map<Integer, FlightRoutes> flightRoutesData = new HashMap<>();
    private Map<Integer, FlightTripStatus> flightTripStatusData = new HashMap<>();

    @Autowired
    private Flight flight;

    public FlightRepository() {
        this.storeFlightData(1, new Flight(1, "Boeing777", 238, new ArrayList<>(Arrays.asList("MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"))));
        this.storeFlightData(2, new Flight(2, "Airbus A321", 172, new ArrayList<>(Arrays.asList("TUE", "THU", "SAT"))));
        this.storeFlightData(3, new Flight(3, "Airbus A319 V2", 144, new ArrayList<>(Arrays.asList("MON", "WED", "FRI", "SUN"))));

        this.storeFlightClassData(1, new FlightClass(1, "Boeing777", "Economy Class", 195));
        this.storeFlightClassData(2, new FlightClass(2, "Boeing777", "Business Class", 35));
        this.storeFlightClassData(3, new FlightClass(3, "Boeing777", "First Class", 8));
        this.storeFlightClassData(4, new FlightClass(4, "Airbus A321", "Economy Class", 152));
        this.storeFlightClassData(5, new FlightClass(5, "Airbus A321", "Business Class", 20));
        this.storeFlightClassData(6, new FlightClass(6, "Airbus A319 V2", "Economy Class", 144));

        this.storeFlightRoutesData(1, new FlightRoutes(1, 1, "HYD", "MUM"));
        this.storeFlightRoutesData(2, new FlightRoutes(2, 2, "HYD", "MUM"));
        this.storeFlightRoutesData(3, new FlightRoutes(3, 3, "HYD", "MUM"));
        this.storeFlightRoutesData(4, new FlightRoutes(4, 1, "HYD", "DEL"));
        this.storeFlightRoutesData(5, new FlightRoutes(5, 2, "HYD", "DEL"));
        this.storeFlightRoutesData(6, new FlightRoutes(6, 3, "HYD", "DEL"));
      /*  this.storeFlightRoutesData(5, new FlightRoutes(5, 2, "MUM", "DEL"));
        this.storeFlightRoutesData(6, new FlightRoutes(6, 2, "DEL", "MUM"));
        this.storeFlightRoutesData(7, new FlightRoutes(7, 3, "DEL", "HYD"));
        this.storeFlightRoutesData(8, new FlightRoutes(8, 3, "MUM", "HYD"));*/

        this.storeFlightTripStatus(1, new FlightTripStatus(1,  1, 1, 2000F, 195, 195));
        this.storeFlightTripStatus(2, new FlightTripStatus(2,  2, 1, 4000F, 35, 35));
        this.storeFlightTripStatus(3, new FlightTripStatus(3,  3, 1, 2000F, 8, 8));
        this.storeFlightTripStatus(4, new FlightTripStatus(4,  4, 2, 4000F, 152, 152));
        this.storeFlightTripStatus(5, new FlightTripStatus(5,  5, 2, 2000F, 20, 20));
        this.storeFlightTripStatus(6, new FlightTripStatus(6,  6, 3, 4000F, 144, 144));

        this.storeFlightTripStatus(7, new FlightTripStatus(7,  1, 4, 2000F, 195, 195));
        this.storeFlightTripStatus(8, new FlightTripStatus(8,  2, 4, 4000F, 35, 35));
        this.storeFlightTripStatus(9, new FlightTripStatus(9,  3, 4, 2000F, 8, 8));
        this.storeFlightTripStatus(10, new FlightTripStatus(10, 4, 5, 4000F, 152, 152));
        this.storeFlightTripStatus(11, new FlightTripStatus(11,  5, 5, 2000F, 20, 20));
        this.storeFlightTripStatus(12, new FlightTripStatus(12,  6, 6, 4000F, 144, 144));

    }

    public FlightRepository(Flight flight) {
        this.flight = flight;
    }

    public void storeFlightClassData(Integer id, FlightClass flightClass) {
        flightClassData.put(id, flightClass);
    }

    public void storeFlightData(Integer id, Flight flight) {
        flightData.put(id, flight);
    }

    public void storeFlightRoutesData(Integer id, FlightRoutes flightRoutes) {
        flightRoutesData.put(id, flightRoutes);
    }

    public void storeFlightTripStatus(Integer id, FlightTripStatus flightTripStatus) {
        flightTripStatusData.put(id, flightTripStatus);
    }

    public Map<Integer, Flight> getFlightData() {
        return flightData;
    }

    public Map<Integer, FlightClass> getFlightClassData() {
        return flightClassData;
    }

    public Map<Integer, FlightRoutes> getFlightRoutesData() {
        return flightRoutesData;
    }

    public Map<Integer, FlightTripStatus> getFlightTripStatusData() {
        return flightTripStatusData;
    }
}

