package com.thoughtworks.vapasi.models;

public class FinalFlightList {

        String flightName;
        Integer availableSeats;
        Float totalCost;
        String className;

        public FinalFlightList(String flightName, Float totalCost, String className, Integer availableSeats)
        {
            this.flightName = flightName;
            this.totalCost = totalCost;
            this.className = className;
            this.availableSeats = availableSeats;
        }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFlightName() {
            return flightName;
        }

        public void setFlightName(String flightName) {
            this.flightName = flightName;
        }

        public Integer getAvailableSeats() {
            return availableSeats;
        }

        public void setAvailableSeats(Integer availableSeats) {
            this.availableSeats = availableSeats;
        }

        public Float getTotalCost() {
            return totalCost;
        }

        public void setTotalCost(Float totalCost) {
            this.totalCost = totalCost;
        }

}

