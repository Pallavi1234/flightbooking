package com.thoughtworks.vapasi.models;

import javax.validation.constraints.NotBlank;
import java.util.Date;

public class SearchCriteria {

    @NotBlank(message = "username can't be empty!")

    private String source;
    private String destination;
    private String numOfPassengers;
    private Date departDate;
    private String classOfTravel;

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getNumOfPassengers() {
        return numOfPassengers;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }
}
