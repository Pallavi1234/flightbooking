package com.thoughtworks.vapasi.models;

import java.util.List;

public class Result {

    private String msg;
    private List<FinalFlightList> flightList;

    public Result(){}

    public void setFlightList(List<FinalFlightList> flightList) {
        this.flightList = flightList;
    }

    public List<FinalFlightList> getFlightList() {
        return flightList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
