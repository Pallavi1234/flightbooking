package com.thoughtworks.vapasi.services;
import com.thoughtworks.vapasi.Repository.FlightRepository;
import com.thoughtworks.vapasi.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FlightSearchServices {

    @Autowired
    FlightRepository flightRepository;

    FlightSearchServices(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public List<FinalFlightList> getFlightDetails(String source, String dest, String numOfPassengers, Date departDate, String classOfTravel) {

        List<String> result = new ArrayList<>();
        List<FinalFlightList>  flightsList = new ArrayList<>();
        Result resultList = new Result();

        if (source.isEmpty() || dest.isEmpty() || source.equals("0") || dest.equals("0")) {
            result.add("Please verify Source city or Destination city");
        } else {
            Map<Integer, Flight> flightData = flightRepository.getFlightData();
            Map<Integer, FlightClass> flightClassData = flightRepository.getFlightClassData();
            Map<Integer, FlightRoutes> flightRoutesData = flightRepository.getFlightRoutesData();
            Map<Integer, FlightTripStatus> flightTripStatusData = flightRepository.getFlightTripStatusData();

            List<Integer> flightIds = new ArrayList<>();
            List<Integer> routeIds = new ArrayList<>();
            for (FlightRoutes routeData : flightRoutesData.values())
                if (routeData.getSourceCity().equals(source))
                    if (routeData.getDestinationCity().equals(dest)) {
                        flightIds.add(routeData.getFlightId());
                        routeIds.add(routeData.getRouteFlightId());
                    }

            if (flightIds.isEmpty())
                return null;

            Integer numberOfPassengers;
            if (!numOfPassengers.isEmpty())
                numberOfPassengers = Integer.parseInt(numOfPassengers.trim());
            else
                numberOfPassengers = 1;

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("E");

            for (Integer id : flightIds) {
                if (flightData.get(id).getAvailableFlyDays().contains((sdf2.format(departDate)).toUpperCase())) {
                    String flightName = flightData.get(id).getFlightName();
                    for (FlightClass classId : flightClassData.values())
                        if (classId.getFlightName().equals(flightName)) {
                            if (classId.getClassName().equals(classOfTravel)) {
                                for (FlightTripStatus tripId : flightTripStatusData.values()) {
                                    int flightClassId = tripId.getFlightClassId();
                                    int routeId = tripId.getFlightRouteId();

                                    if (flightClassId == classId.getClassFlightId()
                                            && routeIds.contains(routeId)) {
                                        if (tripId.getAvailableSeats() >= numberOfPassengers) {
                                            Float bookingCost = calculateCostOfBooking(numberOfPassengers, tripId.getBasePrice(),
                                                    classOfTravel, departDate, tripId.getTotalSeats(), tripId.getAvailableSeats());

                                            System.out.println(flightName + "  " + bookingCost);
                                            result.add("Flight name : " + flightName + "Base Price : " + tripId.getBasePrice() +
                                                    "Total Fare : " + bookingCost + "Available Seats : " + tripId.getAvailableSeats()
                                                    );

                                            flightsList.add(new FinalFlightList(flightName, bookingCost, classOfTravel, tripId.getAvailableSeats()));
                                        }
                                    }
                                }
                                //result.put(flightName, classId.getSeatsForClass());
                            }
                        }
                }
            }
        }
       // return result;
        return flightsList;
    }

    private Float calculateCostOfBooking(Integer numberOfPassengers, Float basePrice, String classOfTravel, Date departDate,
                                         Integer totalSeatsOfClass, Integer availableSeatsOfClass) {

        Float totalBookingCost = 0.0F;

        if (classOfTravel.equals("First Class")) {
            Date today = new Date();
            float diffDays = departDate.getTime() - today.getTime();
            float daysBetween = Math.round(diffDays / (1000 * 60 * 60 * 24));

            totalBookingCost = (basePrice + basePrice * (10 - (int)daysBetween) * 0.1F) * numberOfPassengers;
        } else if (classOfTravel.equals("Business Class")) {
            SimpleDateFormat sdf2 = new SimpleDateFormat("E");
            String day = sdf2.format(departDate).toUpperCase();

            if (day.equals("MON") || day.equals("FRI") || day.equals("SUN"))
                totalBookingCost = (basePrice + (basePrice * 0.4F)) * numberOfPassengers;
            else
                totalBookingCost = basePrice * numberOfPassengers;
        } else if (classOfTravel.equals("Economy Class")) {
            Integer slot1 = (int) (totalSeatsOfClass * 0.4F);
            Integer slot2 = (int) (totalSeatsOfClass * 0.9F) - slot1;
            Integer slot3 = totalSeatsOfClass - (slot1 + slot2);

            Integer availableSlot1 = 0;
            Integer availableSlot2 = 0;
            Integer availableSlot3 = 0;

            if (availableSeatsOfClass >= slot2 + slot3) {
                availableSlot3 = slot3;
                availableSlot2 = slot2;
                availableSlot1 = availableSeatsOfClass - (slot2 + slot3);
            } else if (availableSeatsOfClass >= slot3) {
                availableSlot3 = slot3;
                availableSlot2 = availableSeatsOfClass - slot3;
            } else {
                availableSlot3 = availableSeatsOfClass;
            }

            //Calculate fare
            if (availableSlot1 - numberOfPassengers >= 0)
                totalBookingCost = basePrice * numberOfPassengers;
            else {
                totalBookingCost = basePrice * (availableSlot1);
                numberOfPassengers -= availableSlot1;
                if (numberOfPassengers > availableSlot2) {
                    totalBookingCost += (basePrice + (basePrice * 0.3F)) * availableSlot2;
                    numberOfPassengers -= (availableSlot1 + availableSlot2);

                    if (numberOfPassengers > 0)
                        totalBookingCost += (basePrice + (basePrice * 0.6F)) * numberOfPassengers;
                } else {
                    totalBookingCost += (basePrice + (basePrice * 0.3F)) * numberOfPassengers;
                }
            }
        }

        return totalBookingCost;
    }
}