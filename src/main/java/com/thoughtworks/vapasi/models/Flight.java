package com.thoughtworks.vapasi.models;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Flight {

    private Integer flightID;
    private String flightName;
    private Integer flightCapacity;
    private List<String> availableFlyDays = new ArrayList<>();

    public Flight() {
    }

    public Flight(Integer flightID, String flightName, Integer flightCapacity, List<String> flyDays) {
        this.flightID = flightID;
        this.flightName = flightName;
        this.flightCapacity = flightCapacity;
        this.availableFlyDays = flyDays;
    }

    @Override
    public String toString() {
        return this.flightName + "  " + " "
                + " Capacity:" + this.flightCapacity;
    }

    public String getFlightName() {
        return flightName;
    }

// public Date getDepartDate() {
//   return departDate;
//}

    public List<String> getAvailableFlyDays() {
        return availableFlyDays;
    }

    public Integer getFlightID() {
        return flightID;
    }

    public void setFlightID(Integer flightID) {
        this.flightID = flightID;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public Integer getFlightCapacity() {
        return flightCapacity;
    }

    public void setFlightCapacity(Integer flightCapacity) {
        this.flightCapacity = flightCapacity;
    }

    public void setAvailableFlyDays(List<String> availableFlyDays) {
        this.availableFlyDays = availableFlyDays;
    }

    public Float calculateCurrentPrice() {
        return 1F;
    }

    ;
}
/*
@Component
public class Flight {

    private Integer flightID;
    private String flightName;
    private Integer flightCapacity;
    private String availableFlyDays;

    public void setAvailableFlyDays(String availableFlyDays) {
        this.availableFlyDays = availableFlyDays;
    }

    public void setFlightCapacity(Integer flightCapacity) {
        this.flightCapacity = flightCapacity;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public void setFlightID(Integer flightID) {
        this.flightID = flightID;
    }

    public  Flight(){}

    public Flight(Integer flightID, String flightName, Integer flightCapacity, String flyDays)
    {
        this.flightID = flightID;
        this.flightName = flightName;
       this.flightCapacity = flightCapacity;
        this.availableFlyDays = flyDays;
    }

    @Override
    public String toString()
    {
        return this.flightName + "  " + " Capacity:" + this.flightCapacity;
    }

    public String getFlightName() {
        return flightName;
    }

    public Integer getFlightCapacity() {
        return flightCapacity;
    }

    public String getAvailableFlyDays() {
        return availableFlyDays;
    }

    public Float calculateCurrentPrice(){return 1F;};

}
*/