package com.thoughtworks.vapasi.controller;

import com.thoughtworks.vapasi.models.FinalFlightList;
import com.thoughtworks.vapasi.models.Result;
import com.thoughtworks.vapasi.models.SearchCriteria;
import com.thoughtworks.vapasi.services.FlightSearchServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController {

    @Autowired
    private FlightSearchServices flightSearchServices;


    FlightController(FlightSearchServices flightSearchServices) {

        this.flightSearchServices = flightSearchServices;
    }

    @PostMapping("/api/search")
    public ResponseEntity<?> getSearchResultViaAjax(
            @Valid @RequestBody SearchCriteria searchCriteria, Errors errors) throws ParseException {

        System.out.println(searchCriteria.getDepartDate());

        Result result = new Result();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }

        // Date departDate = new SimpleDateFormat("dd/MM/yyyy").parse(searchFlight.getDepartDate());
        List<FinalFlightList> flights = flightSearchServices.getFlightDetails(searchCriteria.getSource(), searchCriteria.getDestination(),
                searchCriteria.getNumOfPassengers(), searchCriteria.getDepartDate(), searchCriteria.getClassOfTravel());

        if (flights.isEmpty()) {
            result.setMsg("No flights found!");
        } else {
            result.setMsg("");
        }

        result.setFlightList(flights);
        return ResponseEntity.ok(result);

    }
}