package com.thoughtworks.vapasi.models;

public class FlightRoutes {

        Integer routeFlightId;
        Integer flightId;
        String sourceCity;
        String destinationCity;

        public FlightRoutes(Integer routeFlightId, Integer flightId, String sourceCity, String destinationCity)
        {
            this.routeFlightId = routeFlightId;
            this.flightId = flightId;
            this.sourceCity = sourceCity;
            this.destinationCity = destinationCity;
        }
        public Integer getRouteFlightId() {
            return routeFlightId;
        }

        public void setRouteFlightId(Integer routeFlightId) {
            this.routeFlightId = routeFlightId;
        }

        public Integer getFlightId() {
            return flightId;
        }

        public void setFlightId(Integer flightId) {
            this.flightId = flightId;
        }

        public String getSourceCity() {
            return sourceCity;
        }

        public void setSourceCity(String sourceCity) {
            this.sourceCity = sourceCity;
        }

        public String getDestinationCity() {
            return destinationCity;
        }

        public void setDestinationCity(String destinationCity) {
            this.destinationCity = destinationCity;
        }

}

