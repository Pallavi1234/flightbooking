create table demoDatabase.FlightData ( FlightId int(10) not null, FlightName varchar(20) not null );
insert into demoDatabase.employee (employee_id, employee_name) values (1002, 'Shivani');
alter table demoDatabase.FlightData add TotalCapacity int(10);
alter table demoDatabase.FlightData add SourceCity varchar(20);
alter table demoDatabase.FlightData add DestinationCity varchar(20);
alter table demoDatabase.FlightData add primary key (FlightId);
alter table demoDatabase.FlightData drop SourceCity;

alter table demoDatabase.FlightData add FlightId int(10) primary key not null auto_increment;
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Boeing777-200LR(77L)", 238, "WED SAT SUN");
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Boeing777-200LR(77L)", 238, "MON WED FRI SUN");
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Airbus A319 V2", 144, "WED SAT SUN");
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Airbus A319 V2", 144, "MON WED FRI SUN");
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Airbus A321", 172, "TUE THU SUN");
insert into demoDatabase.FlightData (FlightName, TotalCapacity, FlyDays) values ("Airbus A321", 172, "TUE FRI SUN");

create table demoDatabase.Routes(RouteFlightId int(10) primary key not null auto_increment, FlightId int(10), SourceCity varchar(20), DestinationCity varchar(20));
alter table demoDatabase.Routes add unique key (FlightId, SourceCity, DestinationCity);
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (1, "HYD", "MUM");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (2, "HYD", "MUM");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (3, "HYD", "MUM");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (4, "HYD", "MUM");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (1, "MUM", "DEL");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (2, "MUM", "DEL");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (3, "MUM", "DEL");
insert into demoDatabase.Routes (FlightId, SourceCity,DestinationCity) values (4, "MUM", "DEL");

create table demoDatabase.Class(ClassFlightId int(10) primary key not null auto_increment, FlightId int(10), Class varchar(20), TotalSeatsOfClass int(10), AvailableSeatsOfClass int(10));
alter table demoDatabase.Class drop FlightId;
alter table demoDatabase.Class add unique key(FlightName, Class);
alter table demoDatabase.Class add BasePrice int(10);

insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Boeing777-200LR(77L)", "First Class", 8, 20000);
insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Boeing777-200LR(77L)", "Business Class", 35, 13000);
insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Boeing777-200LR(77L)", "Economy Class", 195, 6000);
insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Airbus A319 V2", "Economy Class", 144, 4000);
insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Airbus A321", "Business Class", 20, 10000);
insert into demoDatabase.Class (FlightName, Class, TotalSeatsOfClass, AvailableSeatsOfClass, BasePrice) values("Airbus A321", "Economy Class", 152, 5000);

alter table demoDatabase.Class drop AvailableSeatsOfClass;

select distinct f.FlightName, f.TotalCapacity, f.FlyDays, c.Class, c.TotalSeatsOfClass, c.BasePrice, r.FlightId
from (demoDatabase.FlightData f, demoDatabase.Class c, demoDatabase.Routes r)
inner join demoDatabase.Class
on c.FlightName = f.FlightName
inner join demoDatabase.Routes
on r.FlightId = f.FlightId
where f.FlyDays like '%MON%'
AND r.SourceCity like '%HYD%'
AND r.DestinationCity like '%MUM%'
AND c.Class like '%Economy%'
AND c.TotalSeatsOfClass >= 10;


desc demoDatabase.FlightData;
desc demoDatabase.Routes;
desc demoDatabase.Class;