package com.thoughtworks.vapasi.services;

import com.thoughtworks.vapasi.Repository.FlightRepository;
import com.thoughtworks.vapasi.models.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServicesTest {

    @Mock
    FlightRepository flightRepository;

    private FlightSearchServices flightSearchServices;
    private Map<Integer, Flight> flightData = new HashMap<>();
    private Map<Integer, FlightClass> flightClassData = new HashMap<>();
    private Map<Integer, FlightRoutes> flightRouteData = new HashMap<>();
    private Map<Integer, FlightTripStatus> flightTripStatusData = new HashMap<>();

    @Before
    public void init() {
        flightData.put(1, new Flight(1, "Boeing777", 238, new ArrayList<>(Arrays.asList("MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"))));
        flightData.put(2, new Flight(2, "Airbus A321", 172, new ArrayList<>(Arrays.asList("TUE", "THU", "SAT"))));
        flightData.put(3, new Flight(3, "Airbus A319 V2", 144, new ArrayList<>(Arrays.asList("MON", "WED", "FRI", "SUN"))));

        flightClassData.put(1, new FlightClass(1, "Boeing777", "Economy Class", 195));
        flightClassData.put(2, new FlightClass(2, "Boeing777", "Business Class", 35));
        flightClassData.put(3, new FlightClass(3, "Boeing777", "First Class", 8));
        flightClassData.put(4, new FlightClass(4, "Airbus A321", "Economy Class", 152));
        flightClassData.put(5, new FlightClass(5, "Airbus A321", "Business Class", 20));
        flightClassData.put(6, new FlightClass(6, "Airbus A319 V2", "Economy Class", 144));

        flightRouteData.put(1, new FlightRoutes(1, 1, "HYD", "MUM"));
        flightRouteData.put(2, new FlightRoutes(2, 2, "HYD", "MUM"));
        flightRouteData.put(3, new FlightRoutes(3, 3, "HYD", "MUM"));
        flightRouteData.put(4, new FlightRoutes(4, 1, "HYD", "DEL"));
        flightRouteData.put(5, new FlightRoutes(5, 2, "HYD", "DEL"));
        flightRouteData.put(6, new FlightRoutes(6, 3, "HYD", "DEL"));

        flightTripStatusData.put(1, new FlightTripStatus(1,  1, 1, 2000F, 195, 195));
        flightTripStatusData.put(2, new FlightTripStatus(2,  2, 1, 4000F, 35, 35));
        flightTripStatusData.put(3, new FlightTripStatus(3,  3, 1, 2000F, 8, 8));
        flightTripStatusData.put(4, new FlightTripStatus(4,  4, 2, 4000F, 152, 152));
        flightTripStatusData.put(5, new FlightTripStatus(5,  5, 2, 2000F, 20, 20));
        flightTripStatusData.put(6, new FlightTripStatus(6,  6, 3, 4000F, 144, 144));

        flightTripStatusData.put(7, new FlightTripStatus(7,  1, 4, 2000F, 195, 195));
        flightTripStatusData.put(8, new FlightTripStatus(8,  2, 4, 4000F, 35, 35));
        flightTripStatusData.put(9, new FlightTripStatus(9,  3, 4, 2000F, 8, 8));
        flightTripStatusData.put(10, new FlightTripStatus(10, 4, 5, 4000F, 152, 152));
        flightTripStatusData.put(11, new FlightTripStatus(11,  5, 5, 2000F, 20, 20));
        flightTripStatusData.put(12, new FlightTripStatus(12,  6, 6, 4000F, 144, 144));

        flightSearchServices = new FlightSearchServices(flightRepository);

    }

    @Test
    public void should_ReturnFlightData_When_CalledGetFlightDetails() {
        try {

            Date inputDate = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");

            when(flightRepository.getFlightData()).thenReturn(flightData);
            when(flightRepository.getFlightClassData()).thenReturn(flightClassData);
            when(flightRepository.getFlightRoutesData()).thenReturn(flightRouteData);
            when(flightRepository.getFlightTripStatusData()).thenReturn(flightTripStatusData);

            List<FinalFlightList> actualResult = flightSearchServices.getFlightDetails("HYD", "MUM", "8",
                    inputDate, "First Class");

            List<FinalFlightList> expectedResult = new ArrayList<>();
           // expectedResult.add(new FinalFlightList("Boeing777", 30400F, 2000F,8));

            assertEquals(expectedResult.size(), actualResult.size());

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_ReturnNullList_When_CalledGetFlightDetailsWithNullSource() {

        Date dt = new Date();
        when(flightRepository.getFlightData()).thenReturn(flightData);
        when(flightRepository.getFlightClassData()).thenReturn(flightClassData);
        when(flightRepository.getFlightRoutesData()).thenReturn(flightRouteData);
        when(flightRepository.getFlightTripStatusData()).thenReturn(flightTripStatusData);

        List<FinalFlightList> actualResult = flightSearchServices.getFlightDetails("", "Mumbai", "10", dt, "First Class");
        List<FinalFlightList> expectedResult = new ArrayList<>();

        assertEquals(expectedResult.size(), actualResult.size());
    }

    @Test
    public void should_ReturnNullList_When_CalledGetFlightDetailsWithNullDestination() {

        Date dt = new Date();
        when(flightRepository.getFlightData()).thenReturn(flightData);
        when(flightRepository.getFlightClassData()).thenReturn(flightClassData);
        when(flightRepository.getFlightRoutesData()).thenReturn(flightRouteData);
        when(flightRepository.getFlightTripStatusData()).thenReturn(flightTripStatusData);

       List<FinalFlightList> actualResult = flightSearchServices.getFlightDetails("HYD", "", "10", dt, "First Class");
       List<FinalFlightList> expectedResult = new ArrayList<>();

       assertEquals(expectedResult.size(), actualResult.size());
    }

    @Test
    public void should_ReturnFlightData_When_CalledGetFlightDetailsWithNoPassengerCount() {

        try {

            Date inputDate = new SimpleDateFormat("dd-MM-yyyy").parse("21-08-2019");
            when(flightRepository.getFlightData()).thenReturn(flightData);
            when(flightRepository.getFlightClassData()).thenReturn(flightClassData);
            when(flightRepository.getFlightRoutesData()).thenReturn(flightRouteData);
            when(flightRepository.getFlightTripStatusData()).thenReturn(flightTripStatusData);

            List<FinalFlightList> actualResult = flightSearchServices.getFlightDetails("HYD", "MUM", "", inputDate, "First Class");
            List<Result> expectedResult = new ArrayList<>();

            expectedResult.add(new Result());
            assertEquals(expectedResult, actualResult);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_Return_TotalPrice7600_When_CalledGetFlightDetailsWithRequiredInput()
    {
        Date inputDate = null;
        try {
            inputDate = new SimpleDateFormat("dd-MM-yyyy").parse("27-08-2019");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        when(flightRepository.getFlightData()).thenReturn(flightData);
        when(flightRepository.getFlightClassData()).thenReturn(flightClassData);
        when(flightRepository.getFlightRoutesData()).thenReturn(flightRouteData);
        when(flightRepository.getFlightTripStatusData()).thenReturn(flightTripStatusData);

        List<FinalFlightList> actualResult = flightSearchServices.getFlightDetails("HYD", "DEL", "2", inputDate, "First Class");
        List<Result> expectedResult = new ArrayList<>();

        expectedResult.add(new Result());
    }
}