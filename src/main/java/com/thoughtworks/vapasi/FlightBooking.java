package com.thoughtworks.vapasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightBooking {

    public static void main(String[] args) {

        SpringApplication.run(FlightBooking.class, args);
    }
}
