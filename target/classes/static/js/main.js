
$(document).ready(function () {

    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    var minDate = year + '-' + month + '-' + day;
    var formDate;

    $("#departDate").datepicker({
        minDate: 0,
        dateFormat: "mm/dd/yy",
        onSelect: function(selectedDate){
              formDate = selectedDate;

              var dtSelect = new Date(formDate);

              var businessClassOpenDate = new Date();
              businessClassOpenDate.setDate(dtToday.getDate()+28);
              var firstClassOpenDate = new Date();
              firstClassOpenDate.setDate(dtToday.getDate()+10);

              if(dtSelect > businessClassOpenDate)
                   $('#classOfTravel').children('option[value="2"]').attr('disabled', 'disabled').hide();
              if(dtSelect > firstClassOpenDate)
                  $('#classOfTravel').children('option[value="1"]').attr('disabled', 'disabled').hide();
              }
    });

     //$('#departDate').min(dtToday);

   $("#flight-booking-form").submit(function (event) {
       //stop submit the form event. Do this manually using ajax post function
       event.preventDefault();
       var dtDefault = new Date();
       dtDefault.setDate(dtDefault.getDate()+1);

       var selectedDt = "";
       if(formDate != null && formDate != undefined)
           selectedDt = new Date(formDate);

       //var formDate = document.getElementById('departDate').value;
       var flightBookingForm = {}
       flightBookingForm["source"] = $("#source :selected").val();
       flightBookingForm["destination"] = $("#destination").val();
       flightBookingForm["numOfPassengers"] = $("#numOfPassengers").val() || "1";
       flightBookingForm["departDate"] = selectedDt || dtDefault;
       flightBookingForm["classOfTravel"] = $("#classOfTravel :selected").text() || "Economy Class";
       $("#submit").prop("disabled", true);
       $.ajax({
           type: "POST",
           contentType: "application/json",
           url: "/api/search",
           data: JSON.stringify(flightBookingForm),
           dataType: 'json',
           cache: false,
           timeout: 600000,
           success: function (data) {
               var json = "<h4>Available Flights</h4><pre>"
                   + JSON.stringify(data, null, 4) + "</pre>";

               var flightData = '';
               //var json1 = JSON.parse(data.flightList);
               $.each(data.flightList, function(index, item)
               {
                     flightData += '<tr>';
                     flightData += '<td>' + item.flightName + '</td>';
                     flightData += '<td>' + item.totalCost + '</td>';
                     flightData += '<td>' + item.className + '</td>';
                     flightData += '<td>' + item.availableSeats + '</td>';
                     flightData += '</tr>';
               });

               $('#flight_table').append(flightData);

                if(flightData == "")
                    $('#feedback').html(data.msg);
               console.log("SUCCESS : ", data);
               $("#search").prop("disabled", false);

           },
           error: function (e) {
               var json = "<h4>Ajax Response Error</h4><pre>"
                   + e.responseText + "</pre>";
               $('#feedback').html(json);
               console.log("ERROR : ", e);
               $("#search").prop("disabled", false);
           }
       });
   });
});