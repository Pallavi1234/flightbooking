package com.thoughtworks.vapasi.models;

import org.springframework.beans.factory.annotation.Autowired;

public class FlightTripStatus {

    @Autowired
    FlightRoutes flightRoutes;

    private Integer tripStatusId;
    private Integer flightClassId;
    private Integer flightRouteId;
    private Float basePrice;
    private Integer totalSeats;
    private Integer availableSeats;

    public FlightTripStatus(Integer tripStatusId, Integer flightClassId,
                            Integer flightRouteId, Float basePrice, Integer totalSeats, Integer availableSeats) {
        this.tripStatusId = tripStatusId;
        this.flightClassId = flightClassId;
        this.flightRouteId = flightRouteId;
        this.basePrice = basePrice;
        this.totalSeats = totalSeats;
        this.availableSeats = availableSeats;
    }

    public FlightRoutes getFlightRoutes() {
        return flightRoutes;
    }

    public void setFlightRoutes(FlightRoutes flightRoutes) {
        this.flightRoutes = flightRoutes;
    }

    public Integer getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(Integer tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public Integer getFlightClassId() {
        return flightClassId;
    }

    public void setFlightClassId(Integer flightClassId) {
        this.flightClassId = flightClassId;
    }

    public Integer getFlightRouteId() {
        return flightRouteId;
    }

    public void setFlightRouteId(Integer flightRouteId) {
        this.flightRouteId = flightRouteId;
    }

    public Float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Float basePrice) {
        this.basePrice = basePrice;
    }

    public Integer getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(Integer totalSeats) {
        this.totalSeats = totalSeats;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }
}
