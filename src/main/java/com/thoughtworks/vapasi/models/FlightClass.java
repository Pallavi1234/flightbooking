package com.thoughtworks.vapasi.models;

public class FlightClass {

    Integer classFlightId;
    String flightName;
    String className;
    Integer seatsForClass;

    public FlightClass(Integer classFlightId, String flightName, String className, Integer seatsForClass) {
        this.classFlightId = classFlightId;
        this.flightName = flightName;
        this.className = className;
        this.seatsForClass = seatsForClass;
    }

    public Integer getClassFlightId() {
        return classFlightId;
    }

    public void setClassFlightId(Integer classFlightId) {
        this.classFlightId = classFlightId;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getSeatsForClass() {
        return seatsForClass;
    }

    public void setSeatsForClass(Integer seatsForClass) {
        this.seatsForClass = seatsForClass;
    }
}
